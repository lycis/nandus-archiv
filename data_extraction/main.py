      
import datetime
import sqlite3
import sys
from rich.progress import track
import sqlite3
import requests
from bs4 import BeautifulSoup, Tag
import re
from time import time, sleep
import random

errorUrls = []


TABLE_SPECIES = """
CREATE TABLE IF NOT EXISTS Spezies (
    id            INTEGER       PRIMARY KEY,
    name          VARCHAR (256),
    ap            INTEGER,
    seelenkraft   INT,
    zaehigkeit    INT,
    gs            INT,
    eigenschaften VARCHAR (256),
    vorteile      TEXT,
    nachteile     TEXT,
    referenz      TEXT,
    lebenspunkte  INTEGER,
    wikiLink      VARCHAR (512) 
);
"""

TABLE_SF = """
CREATE TABLE IF NOT EXISTS Sonderfertigkeiten (
    id            INTEGER       PRIMARY KEY,
    name          VARCHAR (256),
    ap            INTEGER,
    regel         TEXT,
    voraussetzung TEXT,
    kategorie     TEXT,
    subkategorie  TEXT,
    referenz      TEXT,
    wikiLink      VARCHAR (512) 
);
"""

TABLE_META = """
CREATE TABLE IF NOT EXISTS Meta (
    key varchar(512) unique,
    value varchar(512)
)
"""

TABLE_CULTURES = """
CREATE TABLE IF NOT EXISTS Kulturen (
    id INTEGER PRIMARY KEY,
    name VARCHAR(256),
    kategorie VARCHAR(256),
    sprache VARCHAR(256),
    schrift VARCHAR(256),
    sozialstatus VARCHAR(256),
    typischeNachteile VARCHAR(512),
    untypischeNachteile VARCHAR(512),
    typischeVorteile VARCHAR(512),
    untypischeVorteile VARCHAR(512),
    referenz TEXT,
    wikiLink VARCHAR(512)
)
"""

TABLE_SPELLS = """
CREATE TABLE IF NOT EXISTS Zauber (
    id INTEGER PRIMARY KEY,
    name VARCHAR(256),
    merkmal VARCHAR(256),
    probe VARCHAR(50),
    ziel VARCHAR(50),
    verbreitung carchar(256),
    kosten VARCHAR(32),
    ritual INTEGER,
    referenz TEXT,
    wikiLink VARCHAR(512)
)
"""

TABLE_ZAUBERERWEITERUNGEN = """
CREATE TABLE IF NOT EXISTS Zaubererweiterungen (
    id INTEGER PRIMARY KEY,
    name varchar(256),
    fw INTEGER,
    ap INTEGER,
    beschreibung TEXT,
    zauber VARCHAR(256)
)
"""

NEXT_REQUEST = 0

def get_url(url: str) -> requests.Response:
    global NEXT_REQUEST

    # firewall and bot scan prevention
    #while time() < NEXT_REQUEST:
    #    sleep(1)

    NEXT_REQUEST = time() + random.randrange(1, 3)

    return requests.get(url, timeout=(10, 10))


def crawl_species() -> list[list]:

    entries = []

    # The URL of the website to be crawled
    url = 'https://www.ulisses-regelwiki.de/spezies.html'

    # Send a GET request to the URL and get the HTML response
    response = get_url(url)

    # Create a BeautifulSoup object to parse the HTML content
    soup = BeautifulSoup(response.text, 'html.parser')

    # Find all the links in the main page
    links = soup.find_all('a')

    # Loop through each link
    for i in track(range(0, len(links)), description="Spezies"):
        link = links[i]
        name = "??"
        ap_wert = "-1"
        vorteile = "keine"
        nachteile = "keine"
        lep = "-1"
        sk = "x"
        zk = "x"
        eigenschaften = ""
        gs = -1
        ref = "fehlt"

        # Get the URL of the subsite
        subsite_url = link.get('href')
        
        # Check if the link is a subsite of the main page
        if subsite_url is not None and subsite_url.startswith("Spez_"):
            # Send a GET request to the subsite and get the HTML response
            targetUrl = "{}/{}".format("https://www.ulisses-regelwiki.de", subsite_url)
            print("scanning: {}".format(targetUrl))
            subsite_response = get_url(targetUrl)
            
            # Create a BeautifulSoup object to parse the HTML content of the subsite
            subsite_soup = BeautifulSoup(subsite_response.text, 'html.parser')
            
            # Extract the name and AP-Wert of the species from the subsite
            main = subsite_soup.find('div', {'id': 'main'})
            if main is None:
                continue

            name = main.find('h1').text.strip()

            ap_wert_elem = main.find('strong', string='AP-Wert')
            if ap_wert_elem is None:
                ap_wert_elem = main.find('strong', string='AP-Wert: ')
                ap_wert = ap_wert_elem.next_sibling.strip().split(" ")[0]
            else:
                ap_wert = ap_wert_elem.next_sibling.strip().split(" ")[1]

            vorteile_elem = main.find('strong', string='Automatischer Vorteil: ')
            if vorteile_elem is None:
                vorteile_elem = main.find('strong', string='Automatische Vorteile:')
            if vorteile_elem is None:
                vorteile_elem = main.find('strong', string='Automatischer Vorteile')

            if vorteile_elem is not None:
                items = [s.string for s in vorteile_elem.next_siblings]
                vorteile = "".join(items).removeprefix(": ")

            nachteile_elem = main.find('strong', string='Automatischer Nachteil: ')
            if nachteile_elem is None:
                nachteile_elem = main.find('strong', string='Automatische Nachteile:')
            if nachteile_elem is None:
                nachteile_elem = main.find('strong', string='Automatische Nachteile')

            if nachteile_elem is not None:
                items = [s.string for s in nachteile_elem.next_siblings]
                nachteile = "".join(items).removeprefix(": ")

            lep_elem  = main.find('strong', string="Lebensenergie-Grundwert: ")
            if lep_elem is None:
                lep_elem  = main.find('strong', string="Lebensenergie-Grundwert")
                lep = lep_elem.next_sibling.strip().split(" ")[1]
            else:
                lep = lep_elem.next_sibling.strip()

            sk_elem  = main.find('strong', string="Seelenkraft-Grundwert: ")
            if sk_elem is None:
                sk_elem  = main.find('strong', string="Seelenkraft-Grundwert")
                sk = sk_elem.next_sibling.strip().split(" ")[1]
            else:
                items = [s.string for s in sk_elem.next_siblings]
                sk = "".join(items)

            zk_elem  = main.find('strong', string="Zähigkeit-Grundwert: ")
            if zk_elem is None:
                zk_elem  = main.find('strong', string="Zähigkeit-Grundwert")
                zk = zk_elem.next_sibling.strip().split(" ")[1]
            else:
                items = [s.string for s in zk_elem.next_siblings]
                zk = "".join(items)

            eigenschaft_elem  = main.find('strong', string="Eigenschaftsänderungen: ")
            if eigenschaft_elem is None:
                eigenschaft_elem  = main.find('strong', string="Eigenschaftsänderungen")
            
            items = [s.string for s in eigenschaft_elem.next_siblings]
            eigenschaften = "".join(items).removeprefix(": ")

            gs_elem = main.find('strong', string='Geschwindigkeit-Grundwert')
            if gs_elem is None:
                gs_elem = main.find('strong', string='Geschwindigkeit-Grundwert: ')
            
            if gs_elem is not None:
                items = [s.string for s in gs_elem.next_siblings]
                gs = "".join(items).removeprefix(": ")

            
            ref = extract_reference(main)
            if ref is None:
                for div in main.find_all('div', {"class": "ce_text"}):
                    ref = extract_reference(div)
                    if ref is not None:
                        break
            

            entries.append([name, ap_wert, sk, zk, gs, eigenschaften, vorteile, nachteile, ref, lep, subsite_url])
    return entries

def extract_reference(main: Tag) -> str:
    ref = None

    ref_elem = main.find(ref_finder_strong)
    if ref_elem is None:
        ref_elem = main.find(ref_finder_p)
    if ref_elem is None:
        ref_elem = main.find(ref_finder_div)
    if ref_elem is not None:
        if ref_elem.name == 'p':
            ref = ref_elem.text.split(":")[1]
        else:
            ref = ""
            for sibling in ref_elem.next_siblings:
                if sibling.string is not None:
                    ref += sibling.string+" "
    return ref

def ref_finder_strong(tag) -> bool:
    return ref_finder(tag, 'strong')

def ref_finder_p(tag) -> bool:
    return ref_finder(tag, 'p')

def ref_finder_div(tag) -> bool:
    return ref_finder(tag, 'div')

def ref_finder(tag, tagType: str) -> bool:
    if len(tag.contents) <= 0:
        return False
    
    if tag.name == tagType and matches_ref_entry(tag):
        return True
    
    #tag.name == "p" or tag.name == "div" or
    return False

def matches_ref_entry(tag) -> bool:
    for p in ["Publikationen:", 'Publikation(en)', 'Publikation(en):', 'Publikation', 'Publikation:']:
            if p in tag.contents[0]:
                return True
    return False
        
def insert_species(species: list[list], connection: sqlite3.Connection):
    crsr = connection.cursor()
    for s in species:
        try:
            crsr.execute("""INSERT INTO Spezies(name, ap, seelenkraft, zaehigkeit, gs, eigenschaften, vorteile, nachteile, referenz, lebenspunkte, wikiLink)
                                VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                            """, s)
        except sqlite3.Error as e:
            print("Error while writing species: {}".format(s))
            connection.rollback()
            
    connection.commit()


def crawl_zaubersprueche(con: sqlite3.Connection) -> list[list]:
    return crawl_spells("https://www.ulisses-regelwiki.de/ritualauswahl.html", True) + crawl_spells("https://www.ulisses-regelwiki.de/zauberauswahl.html", False)

def crawl_spells(url: str, isRitual: bool) -> list[list]:
    entries = []
    response = get_url(url)

    soup = BeautifulSoup(response.text, 'html.parser')
    links = soup.find_all('a')

    for i in track(range(0, len(links)), description="Zaubersprüche"):
        link = links[i].get('href')
        if link is None:
            continue
        if not link.startswith('zauber.html?') and not link.startswith("ritual.html?"):
            continue

        print("zauber: {}".format(link))
        targetUrl = "{}/{}".format("https://www.ulisses-regelwiki.de", link) 
        spell_response = get_url(targetUrl)
        spell_soup = BeautifulSoup(spell_response.text, 'html.parser')
            
        main = spell_soup.find('div', {'id': 'main'})
        if main is None:
            continue


        name = main.find('div', {"class": "header"}).text.strip()
        referenz = extract_sf_entry_value(main, ["Publikation(en):", "Publikation(en): "])
        merkmal = extract_sf_entry_value(main, ["Merkmal:"])
        probe = extract_sf_entry_value(main, ["Probe: "])
        ziel = extract_sf_entry_value(main, ["Zielkategorie:"])
        verbreitung = extract_sf_entry_value(main, ["Verbreitung:"])
        kosten = extract_sf_entry_value(main, ["AsP-Kosten:", "AsP-Kosten: "])

        extract_zaubererweiterungen(main, con, name)

        entries.append([name, merkmal, probe, ziel, verbreitung, kosten, isRitual, referenz, link])

    return entries

def extract_zaubererweiterungen(main: Tag, con: sqlite3.Connection, zauber: str):
    # Find the initial div with class "body_einzeln" containing "Zaubererweiterungen"
    zauberer_div = main.find("div", class_="body_einzeln", string="Zaubererweiterungen")

    if zauberer_div:
        next_div = zauberer_div.find_next("div")

        while next_div:
            if next_div.get('class') is not None and "body_einzeln" in next_div.get("class"):
                break
            
            if next_div is not None:
                header = next_div.get_text(strip=True)
                if header is not None:
                    header = header.strip("# :")
                    regex_pattern = r"\s*(.*?)\(\s*FW\s+(\d+),\s*(\d+)\s*AP\)"
                    match = re.search(regex_pattern, header)
                    if match:
                        # Extract the captured groups
                        name = match.group(1)
                        fw = match.group(2)
                        ap = match.group(3)
                    else:
                        print("Pattern not found in the input string.")


            next_div = next_div.find_next("div")
            if next_div is not None:
                description = next_div.get_text(strip=True)
                print("name:", name)
                print("fw:", fw)
                print("ap:", ap)
                print("Description:", description)
                print()

                # Insert the data into the SQLite database
                cursor = con.cursor()
                cursor.execute("INSERT INTO Zaubererweiterungen (name, fw, ap, beschreibung, zauber) VALUES (?, ?, ?, ?, ?)", (name, fw, ap, description, zauber))
                con.commit()

            if next_div is None:
                break
            next_div = next_div.find_next("div")


def insert_zaubersprueche(spells: list[list], connection: sqlite3.Connection):
    crsr = connection.cursor()
    for spell in spells:
        try:
            crsr.execute("""INSERT INTO Zauber(name, merkmal, probe, ziel, verbreitung, kosten, ritual, referenz, wikiLink)
                                VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)
                            """, spell)
        except sqlite3.Error as e:
            print("Error '{}' while writing spell: {}".format(e, spell))
            connection.rollback()
            return
            
    connection.commit()


def crawl_cultures() -> list[list]:
    entries = []
    # The URL of the website to be crawled
    url = 'https://www.ulisses-regelwiki.de/kulturen.html'

    # Send a GET request to the URL and get the HTML response
    response = get_url(url)

    # Create a BeautifulSoup object to parse the HTML content
    soup = BeautifulSoup(response.text, 'html.parser')

    # Find all the links in the main page
    links = soup.find_all('a')

    # Loop through each link
    for i in track(range(0, len(links)), description="Kluturen"):
        link = links[i]
        # Get the URL of the subsite
        subsite_url = link.get('href')
        
        # Check if the link is a subsite of the main page
        if subsite_url is None:
            continue

        if not subsite_url.startswith("Kul_"):
            continue

        # Send a GET request to the subsite and get the HTML response
        targetUrl = "{}/{}".format("https://www.ulisses-regelwiki.de", subsite_url)
        print("scanning: {}".format(targetUrl))
        subsite_response = get_url(targetUrl)
        
        # Create a BeautifulSoup object to parse the HTML content of the subsite
        subsite_soup = BeautifulSoup(subsite_response.text, 'html.parser')
        sublinks = subsite_soup.find_all('a', {'class': 'ulSubMenu'})
        for sl in sublinks:
            if sl.get('href') is None:
                continue
            if not sl.get('href').startswith("Kul_"):
                    continue
            
            print("Scanning culture '{}".format(sl))
            targetUrl = "{}/{}".format("https://www.ulisses-regelwiki.de", sl.get('href')) 
            culture_response = get_url(targetUrl)
            culture_soup = BeautifulSoup(culture_response.text, 'html.parser')
            
            main = culture_soup.find('div', {'id': 'main'})
            if main is None:
                continue

            name = main.find('h1').text.strip()
            sprache = extract_sf_entry_value(main, ["Sprache", "Sprache:", "Sprache: "])
            schrift = extract_sf_entry_value(main, ["Schrift", "Schrift:", "Schrift: "])
            sozialstatus = extract_sf_entry_value(main, ["Sozialstatus", "Sozialstatus:", "Sozialstatus: "])
            typischeNachteile = extract_sf_entry_value(main, ["Typische Nachteile", "Typische Nachteile:", "Typische Nachteile: "])
            untypischeNachteile = extract_sf_entry_value(main, ["Untypische Nachteile", "Untypische Nachteile:", "Untypische Nachteile: "])
            typischeVorteile = extract_sf_entry_value(main, ["Typische Vorteile", "Typische Vorteile:", "Typische Vorteile: "])
            untypischeVorteile = extract_sf_entry_value(main, ["Untypische Vorteile", "Untypische Vorteile:", "Untypische Vorteile: "])
            ref = extract_sf_entry_value(main, ["Publikation(en):"])
            if ref is not None:
                ref = ref.strip()
            entries.append([name, subsite_url.split('_')[1].split('.')[0], sprache, schrift, sozialstatus, typischeNachteile, untypischeNachteile, typischeVorteile, untypischeVorteile, ref, sl.get('href')])
    return entries


def insert_cultures(cultures: list[list], connection: sqlite3.Connection):
    crsr = connection.cursor()
    for culture in cultures:
        try:
            crsr.execute("""INSERT INTO Kulturen(name, kategorie, sprache, schrift, sozialstatus, typischeNachteile, untypischeNachteile, typischeVorteile, untypischeVOrteile, referenz, wikiLink)
                                VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                            """, culture)
        except sqlite3.Error as e:
            print("Error '{}' while writing culture: {}".format(e, culture))
            connection.rollback()
            return
            
    connection.commit()

def set_sf_buildstep(n: int, con: sqlite3.Connection):
    crsr = con.cursor()

    try:
        crsr.execute("INSERT OR IGNORE INTO Meta VALUES('sfBuildStep', 0)")
        crsr.execute("UPDATE Meta SET value = ? where key = 'sfBuildStep'", [n])
        con.commit()
    except sqlite3.Error as e:
        print("Database error when updating sf buildstep: {}".format(e))
        con.rollback()

def get_sf_buildstep(con: sqlite3.Connection) -> int:
    crsr = con.cursor()

    crsr.execute("SELECT value from Meta WHERE key = 'sfBuildStep'")
    rows = crsr.fetchall()

    if len(rows) < 1:
        return 0

    return int(rows[0][0])


def build_zeremonialggst_list():
    entries = {}

    print("scanning sf subcategory: ", "https://www.ulisses-regelwiki.de/zeremonialgegenstands_sf_auswahl.html")

    # Send a GET request to the URL and get the HTML response
    response = get_url( "https://www.ulisses-regelwiki.de/zeremonialgegenstands_sf_auswahl.html")

    # Create a BeautifulSoup object to parse the HTML content
    soup = BeautifulSoup(response.text, 'html.parser')

    # Find all the links in the main page
    items = soup.find('select', {"name": "zeremonialgegenstand"}).find_all("option")

    # Loop through each link
    for i in items:
        if "disabled" in i.attrs or i.attrs["value"] == "*":
            continue
        entries[i.string] = (crawl_sf_subcategory, "https://www.ulisses-regelwiki.de/zeremonialgegenstands_sf_auswahl.html?zeremonialgegenstand={}".format(i.attrs['value']))
    return entries

def crawl_sf(con: sqlite3.Connection) -> list[list]:
    entries = {
        "Karmal": {
            "Allgemein": (crawl_sf_subcategory, "https://www.ulisses-regelwiki.de/allgemeine_karmale_sonderfertigkeitauswahl.html"),
            "Erweiterte Liturgiesonderfertigkeit": (crawl_sf_subcategory, "https://www.ulisses-regelwiki.de/erweiterte_liturgiestilsonderfertigkeitenauswahl.html"),
            "Tradition": (crawl_sf_subcategory,"https://ulisses-regelwiki.de/karmale_traditionauswahl.html"),
            "Liturgiestil": (crawl_sf_subcategory, "https://ulisses-regelwiki.de/liturgiestilsonderfertigkeitenauswahl.html"),
            "Predigt": (crawl_sf_subcategory, "https://ulisses-regelwiki.de/predigtauswahl.html"),
           "Visions": (crawl_sf_subcategory, "https://ulisses-regelwiki.de/visionauswahl.html")
        },
        "Zeremonialgegenstand": build_zeremonialggst_list(),
        "Magisch": {
            "Allgemein": (crawl_sf_subcategory, "https://ulisses-regelwiki.de/allgemeine_magische_sonderfertigkeitauswahl.html"),
            "Erweiterte Zaubersonderfertigkeit": (crawl_sf_subcategory, "https://ulisses-regelwiki.de/erw_zauber_sf_auswahl.html"),
            "Homunculusfähigkeit": (crawl_sf_subcategory, "https://ulisses-regelwiki.de/msf_Homunculusfaehigkeiten.html"),
            "Lykanthropische Gabe": (crawl_zauberzeichen, "https://ulisses-regelwiki.de/lykanthropische-gaben.html"),
            "Tradition": (crawl_sf_subcategory, "https://ulisses-regelwiki.de/magische_traditionauswahl.html"),
            "Sikaryan-Raub": (crawl_zauberzeichen, "https://ulisses-regelwiki.de/sikaryan-raub-sonderfertigkeiten.html"),
            "Vampirische Gabe": (crawl_zauberzeichen, "https://ulisses-regelwiki.de/VampirischeGaben.html"),
            "Zauberstil": (crawl_sf_subcategory, "https://ulisses-regelwiki.de/zauberstil_sf_auswahl.html")
        },
        "Traditionsartefakte": {
            "Animistenwaffe": (crawl_traditionsartefakt, "https://ulisses-regelwiki.de/Animistenwaffe.html"),
            "Bannschwert": (crawl_traditionsartefakt, "https://ulisses-regelwiki.de/traditionsartefakt_sf_bannschwertzauber_auswahl.html"),
            "Schuppenbeutel": (crawl_traditionsartefakt, "https://ulisses-regelwiki.de/traditionsartefakt_sf_beutelzauber_auswahl.html"),
            "Druidendolch": (crawl_traditionsartefakt, "https://ulisses-regelwiki.de/traditionsartefakt_sf_dolchritual_auswahl.html"),
            "Druidensichel": (crawl_traditionsartefakt, "https://ulisses-regelwiki.de/traditionsartefakt_sf_sichelritual_auswahl.html"),
            "Fluggerät": (crawl_traditionsartefakt, "https://ulisses-regelwiki.de/traditionsartefakt_sf_fluggeraetritual_auswahl.html"),
            "Gewandzauber": (crawl_traditionsartefakt, "https://ulisses-regelwiki.de/traditionsartefakt_sf_gewandzauber_auswahl.html"),
            "Gildenmagische Kugelzauber": (crawl_traditionsartefakt, "https://ulisses-regelwiki.de/traditionsartefakt_sf_magierkugelzauber_auswahl.html"),
            "Echsenhaube": (crawl_traditionsartefakt, "https://ulisses-regelwiki.de/traditionsartefakt_sf_haubenzauber_auswahl.html"),
            "Instrumentzauber": (crawl_traditionsartefakt, "https://ulisses-regelwiki.de/traditionsartefakt_sf_instrumentzauber_auswahl.html"),
            "Kappenzauber": (crawl_traditionsartefakt, "https://ulisses-regelwiki.de/traditionsartefakt_sf_kappenzauber_auswahl.html"),
            "Kesselzauber": (crawl_traditionsartefakt, "https://ulisses-regelwiki.de/traditionsartefakt_sf_kesselzauber_auswahl.html"),
            "Krallenkette": (crawl_traditionsartefakt, "https://ulisses-regelwiki.de/traditionsartefakt_sf_krallenkettenzauber_auswahl.html"),
            "Kristallomantische Kugel": (crawl_traditionsartefakt, "https://ulisses-regelwiki.de/traditionsartefakt_sf_kristallomantkugelzauber_auswahl.html"),
            "Lebensring": (crawl_traditionsartefakt, "https://ulisses-regelwiki.de/traditionsartefakt_sf_ringzauber_auswahl.html"),
            "Schalenzauber": (crawl_traditionsartefakt, "https://ulisses-regelwiki.de/traditionsartefakt_sf_schalenzauber_auswahl.html"),
            "Scharlatanische Kugelzauber": (crawl_traditionsartefakt, "https://ulisses-regelwiki.de/traditionsartefakt_sf_scharlatankugelzauber_auswahl.html"),
            "Sippenchronik": (crawl_traditionsartefakt, "https://ulisses-regelwiki.de/traditionsartefakt_sf_chronikzauber_auswahl.html"),
            "Spielzeugzauber": (crawl_traditionsartefakt, "https://ulisses-regelwiki.de/traditionsartefakt_sf_spielzeugzauber_auswahl.html"),
            "Stabzauber": (crawl_traditionsartefakt, "https://ulisses-regelwiki.de/traditionsartefakt_sf_stabzauber_auswahl.html"),
            "Steckenzauber": (crawl_traditionsartefakt, "https://ulisses-regelwiki.de/traditionsartefakt_sf_steckenzauber_auswahl.html"),
            "Trinkhornzauber": (crawl_traditionsartefakt, "https://ulisses-regelwiki.de/traditionsartefakt_sf_trinkornzauber_auswahl.html"),
        },
        "Paktgeschenk": {
            "Dämonenpakt": (crawl_zauberzeichen, "https://www.ulisses-regelwiki.de/Daemonenpakt.html"),
            "Feenpakt": (crawl_zauberzeichen, "https://www.ulisses-regelwiki.de/Feenpakt.html")
        },
        "Zauberzeichen": {
            "Ahnenzeichen": (crawl_zauberzeichen, "https://ulisses-regelwiki.de/Ahnenzeichen.html"),
            "Alhanishe Zauberzeichen": (crawl_zauberzeichen, "https://ulisses-regelwiki.de/mSF-AlhanischeZauberzeichen.html"),
            "Bann- und Schutzkreise": (crawl_zauberzeichen, "https://ulisses-regelwiki.de/bannundschutz.html"),
            "Zauberzeichen": (crawl_zauberzeichen, "https://ulisses-regelwiki.de/zz_Zauberzeichen.html")
        },
        "Profan": {
            "Allgemein": (crawl_zauberzeichen, "https://ulisses-regelwiki.de/sf_allgemeine_sonderfertigkeiten.html"),
            "Befehl": (crawl_zauberzeichen, "https://ulisses-regelwiki.de/Befehls_SF.html"),
            "Erweiterte Talentsonderfertigkeit": (crawl_zauberzeichen, "https://ulisses-regelwiki.de/Erweiterte_Talentsonderfertigkeiten.html"),
            "Schicksalspunkte": (crawl_zauberzeichen, "https://ulisses-regelwiki.de/SF_Schick.html"),
            #"Sprachen und Schriften": (crawl_zauberzeichen, "https://ulisses-regelwiki.de/Sprachen.html"),
            "Talentstil": (crawl_zauberzeichen, "https://ulisses-regelwiki.de/Talentstilsonderfertigkeiten.html"),
            "Erweiterte Kampfsonderfertigkeit": (crawl_zauberzeichen, "https://ulisses-regelwiki.de/SF_Erweitertekampfstilsonderfertigkeiten.html"),
            "Kampfstil": (crawl_zauberzeichen, "https://ulisses-regelwiki.de/SF_Kampfstilsonderfertigkeiten.html"),
            "Kampf": (crawl_zauberzeichen, "https://ulisses-regelwiki.de/sf_kampfsonderfertigkeiten.html"),
            "Prügel": (crawl_zauberzeichen, "https://www.ulisses-regelwiki.de/SF_Pruegelsonderfertigkeiten.html")
        },
        "Tiere": {
            "Ausbildungsaufsatz": (crawl_zauberzeichen, "https://www.ulisses-regelwiki.de/TSF_Ausbildungsaufs%C3%A4tze.html"),
            "Humunculus": (crawl_zauberzeichen, "https://www.ulisses-regelwiki.de/sf_homunculus.html"),
            "Kampf": (crawl_zauberzeichen, "https://www.ulisses-regelwiki.de/TSF_Kampfsonderfertigkeiten.html"),
            "Allgemein": (crawl_zauberzeichen, "https://www.ulisses-regelwiki.de/SF_Tiersonderfertigkeiten.html"),
            "Trick": (crawl_zauberzeichen, "https://www.ulisses-regelwiki.de/TSF_Tricks.html"),
            "Vertrautentrick": (crawl_zauberzeichen, "https://www.ulisses-regelwiki.de/VSF_Vertrautentricks.html")
        }
    }

    result = []
    step = 1
    for (category, v) in entries.items():
        for (subcategory, x) in v.items():
            if get_sf_buildstep(con) < step:
                result = []
                for sf in x[0](x[1]):
                    if len(sf) < 1:
                        print("Error: No data for SF")
                    else:
                        result.append([*sf, category, subcategory])
                insert_sf(result, con)
                set_sf_buildstep(step, con)
            else:
                print("{}/{}: Already completed.".format(category, subcategory))
            step = step + 1
    return result

def insert_sf(all_sf: list[list], connection: sqlite3.Connection):
    crsr = connection.cursor()
    for sf in all_sf:
        try:
            crsr.execute("""INSERT INTO Sonderfertigkeiten(name, ap, voraussetzung, regel, referenz, wikiLink, kategorie, subkategorie)
                                VALUES(?, ?, ?, ?, ?, ?, ?, ?)
                            """, sf)
        except sqlite3.Error as e:
            print("Error '{}' while writing sonderfertigkeiten: {}".format(e, sf))
            connection.rollback()
            return
            
    connection.commit()

def crawl_sf_category(url: str) -> dict:
    entries = {}

    print("scanning sf category: ", url)

    # Send a GET request to the URL and get the HTML response
    response = get_url(url)

    # Create a BeautifulSoup object to parse the HTML content
    soup = BeautifulSoup(response.text, 'html.parser')

    links = soup.find_all('a', {"class":"ulSubMenu"})

    # Loop through each link
    for i in track(range(0, len(links)), description="SF: {}".format(url)):
        link = links[i]
        entries[ link.string] = crawl_zauberzeichen("https://www.ulisses-regelwiki.de/{}".format(link.get('href')), True)
       
    return entries

def crawl_sf_subcategory(url: str, finder=None) -> list:
    entries = []

    print("scanning sf subcategory: ", url)

    # Send a GET request to the URL and get the HTML response
    response = get_url(url)

    # Create a BeautifulSoup object to parse the HTML content
    soup = BeautifulSoup(response.text, 'html.parser')

    # Find all the links in the main page
    links = soup.find('div', id="main").find_all('a')
    #print(links)

    # Loop through each link
    for i in track(range(0, len(links)), description="SF: {}".format(url)):
        link = links[i]
        name = None
        ap = None
        requ = "<keine>"
        rule = None
        ref = "fehlt"

        # Get the URL of the subsite
        subsite_url = link.get('href')
        if subsite_url == "Fokus_Predigten_Visionen.html" or subsite_url == "Zeremonialgegenstaende.html" or subsite_url == "Zauberstilsonderfertigkeiten.html" or subsite_url == "Fokus_Zauberstile.html":
            continue
        
        targetUrl = "{}/{}".format("https://www.ulisses-regelwiki.de", subsite_url)
        print("scanning sf: {}".format(targetUrl))
        subsite_response = get_url(targetUrl)
        
        # Create a BeautifulSoup object to parse the HTML content of the subsite
        subsite_soup = BeautifulSoup(subsite_response.text, 'html.parser')
        
        # Extract the name and AP-Wert of the species from the subsite
        main = subsite_soup.find('div', {'id': 'main'})
        if main is None:
            continue

        name = main.find('div', {"class": "header"}).string.replace("\n", "").strip()
        try:
            ap   = int(extract_sf_entry_value(main, ["AP-Wert:"]).split(" ")[0])
        except ValueError:
            ap = -1    
        requ = extract_sf_entry_value(main, ["Voraussetzung:", "Voraussetzungen:"])
        if requ is None:
            requ = "<keine>"
        else:
            requ = requ.strip()
        #rule = extract_sf_entry_value(main, ["Regel:"]).strip()
        rule = "<redacted>"
        ref = extract_sf_entry_value(main, ["Publikation(en):"])
        if ref is not None:
            ref = ref.strip()

        if name is None or ap is None or requ is None or rule is None:
            print("-- Error --")
            print("    Name:", name)
            print("    AP-Kosten:", ap)
            print("    Voraussetzungen:", requ)
            print("    Regel:", rule)
            print("    Referenz:", ref)
            print("-- End --")
            errorUrls.append(["SF", targetUrl])
        else:
            entries.append([name, ap, requ, rule, ref, subsite_url])

    return entries

def crawl_zauberzeichen(url: str, embedded=False) -> list:
    entries = []

    print("scanning zauberzeichen: ", url)

    # Send a GET request to the URL and get the HTML response
    response = get_url(url)

    # Create a BeautifulSoup object to parse the HTML content
    soup = BeautifulSoup(response.text, 'html.parser')

    # Find all the links in the main page
    links = soup.find_all('a', {"class": "ulSubMenu"})

    # Loop through each link
    if embedded:
        x = range(len(links))
    else:
        x = track(range(0, len(links)), description="SF: {}".format(url))
    for i in x:
        link = links[i]
        subsite_url = link.get('href')
        r = extract_sf_data_2(subsite_url, link.string.strip())
        if r is not None:
            entries.append(r)

    return entries


def crawl_traditionsartefakt(url: str, embedded=False) -> list:
    entries = []

    print("scanning traditionsartefakt: ", url)

    # Send a GET request to the URL and get the HTML response
    response = get_url(url)

    # Create a BeautifulSoup object to parse the HTML content
    soup = BeautifulSoup(response.text, 'html.parser')

    # Find all the links in the main page
    links = soup.find_all('a')

    # Loop through each link
    if embedded:
        x = range(len(links))
    else:
        x = track(range(0, len(links)), description="SF: {}".format(url))
    for i in x:
        link = links[i]
        subsite_url = link.get('href')
        if subsite_url is None:
            print("-> none")
            continue
        if not subsite_url.startswith(url.split("/")[-1].split(".")[0][0:-8]):
            print("not in subsite ({} != {})".format(subsite_url, url.split("/")[-1].split(".")[0][0:-8])) 
            continue
        if not "?" in subsite_url:
            print("no ?")
            continue
        if "#skipNavigation" in subsite_url:
            print("skip")
            continue
        r = extract_sf_data_2(subsite_url, link.string.strip())
        if r is not None:
            entries.append(r)

    return entries

def extract_sf_data_2(subsite_url, name: str) -> list:
    #name = None
    ap = -1
    requ = "<keine>"
    rule = None
    ref = "fehlt"
    
    targetUrl = "{}/{}".format("https://www.ulisses-regelwiki.de", subsite_url)
    print("scanning sf: {}".format(targetUrl))
    subsite_response = get_url(targetUrl)
    
    # Create a BeautifulSoup object to parse the HTML content of the subsite
    subsite_soup = BeautifulSoup(subsite_response.text, 'html.parser')
    
    # Extract the name and AP-Wert of the species from the subsite
    main = subsite_soup.find('div', {'id': 'main'})
    if main is None:
        return None

    try:
        value = extract_sf_entry_value(main, ["AP-Wert:", "AP-Wert", "AP-Wert ", "AP-Wert: "])
        if value is not None:
            ap   = int(value.split(" ")[0])
    except ValueError:
        ap = -1   

    ref = extract_reference(main)
    if ref is None:
        for div in main.find_all('div', {"class": "ce_text"}):
            ref = extract_reference(div)
            if ref is not None:
                break
    if ref is None:
         for div in main.find_all('div', {"class": "spalte1"}):
            ref = extract_reference(div)
            if ref is not None:
                break
            

    requ = extract_sf_entry_value(main, ["Voraussetzung:", "Voraussetzungen:", "Voraussetzungen", "Voraussetzungen: "])
    if requ is None:
        requ = "<keine>"
    else:
        requ = requ.strip()

    return [name, ap, requ, rule, ref, subsite_url]

def extract_sf_entry_value(parent, nameVariations: list[str]) -> str:
    for name in nameVariations:
        for elem in  parent.find_all('div', {"class":"spalte1"}):
            if elem.string == name:
                if elem is not None and elem.find_next() is not None:
                    items = [s.string or "" for s in elem.find_next().children]
                    return "".join(items).removeprefix(": ")
        for elem in parent.find_all('strong', string=name):
            if elem is not None and elem.next_sibling is not None:
                items = [s or "" for s in elem.next_sibling]
                return "".join(items).removeprefix(": ")
        for elem in parent.find_all('em', string=name):
            if elem is not None and elem.next_sibling is not None:
                items = [s or "" for s in elem.next_sibling]
                return "".join(items).removeprefix(": ")

    return None

def connect_db(file: str) -> sqlite3.Connection:
    connection = None
    try:
        connection = sqlite3.connect(file)
        crsr = connection.cursor()

        crsr.execute(TABLE_SPECIES)
        crsr.execute(TABLE_SF)
        crsr.execute(TABLE_META)
        crsr.execute(TABLE_CULTURES)
        crsr.execute(TABLE_SPELLS)
        crsr.execute(TABLE_ZAUBERERWEITERUNGEN)
    
        connection.commit()

    except sqlite3.Error as e:
        print("Database error during creation: {}".format(e))
        connection.rollback()
        return None
    
    return connection


def insert_metadata(con: sqlite3.Connection):
    print("Writing Metadata:")
    crsr = con.cursor()
    try:
        s = str(datetime.datetime.now())
        print("    Build Date:", s)
        crsr.execute("INSERT INTO Meta VALUES(?, ?);", ['buildDate', s])
        con.commit()
    except sqlite3.Error as e:
        print("Database error during metadata: {}".format(e))
        con.rollback()

def set_buildstep(n: int, con: sqlite3.Connection):
    crsr = con.cursor()

    try:
        crsr.execute("INSERT OR IGNORE INTO Meta VALUES('buildStep', 0)")
        crsr.execute("UPDATE Meta SET value = ? where key = 'buildStep'", [n])
        con.commit()
    except sqlite3.Error as e:
        print("Database error when updating buildstep: {}".format(e))
        con.rollback()

def get_buildstep(con: sqlite3.Connection) -> int:
    crsr = con.cursor()

    crsr.execute("SELECT value from Meta WHERE key = 'buildStep'")
    rows = crsr.fetchall()

    if len(rows) < 1:
        return 0

    return int(rows[0][0])

if __name__ == "__main__":
    con = connect_db(sys.argv[1])

    if get_buildstep(con) < 1:
        set_buildstep(0, con)
        for i in range(random.randrange(5, 10), 0, -1):
            print("Starting Spezies Download in: {} sec".format(i), end = '\r')
            sleep(1)
            
        species = crawl_species()
        insert_species(species, con)
        set_buildstep(1, con)
    else:
        print("Species: Already completed.")

    if get_buildstep(con) < 2:
        for i in range(random.randrange(5, 10), 0, -1):
            print("Starting SF Download in: {} sec".format(i), end = '\r')
            sleep(1)

        sf = crawl_sf(con)
        set_buildstep(2, con)
    else:
        print("Sonderfertigkeiten: Already completed.")

    if get_buildstep(con) < 3:
        for i in range(random.randrange(5, 10), 0, -1):
            print("Starting Culture Download in: {} sec".format(i), end = '\r')
            sleep(1)

        cultures = crawl_cultures()
        insert_cultures(cultures, con)
        set_buildstep(3)
    else:
        print("Cultures: Already completed.")

    if get_buildstep(con) < 4:
        for i in range(random.randrange(5, 10), 0, -1):
            print("Starting Zauber Download in: {} sec".format(i), end = '\r')
            sleep(1)

        spells = crawl_zaubersprueche(con)
        insert_zaubersprueche(spells, con)
        set_buildstep(4, con)
    else:
        print("Zauber: Already completed.")

    insert_metadata(con)

    if len(errorUrls):
        print("Error in URL:", errorUrls)
    
    print("[Done]")