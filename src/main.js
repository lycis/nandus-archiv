import { createApp } from 'vue'
import { createPinia } from 'pinia'
import SQL from 'sql.js'

import App from './App.vue'
import router from './router'

import Vuex from 'vuex'
import axios from 'axios'

const app = createApp(App)

app.use(Vuex)
//app.use(VueResource)

app.config.productionTip = false

const store = new Vuex.Store({
  state: {
    dbFile: null,
    db: null,
    values: []
  },
  mutations: {
    increment (state) {
      state.count++
    },
    GET_DBFILE (state, dbFile) {
      console.log('begin GET_DBFILE mutations in store')
      this.state.dbFile = dbFile
      console.log('this length ' + this.state.dbFile.length)
      console.log('end GET_DBFILE mutations in store')
    }
  },
  actions: {
    getDBFile ({ commit }, { self }) {
      console.log('begin getDBFile')
      // Vue.http.get('http://localhost:8080/assets/mytest.db', {responseType: 'arraybuffer'})
      let dbUrl =   process.env.NODE_ENV === 'development' ?  '/data.sqlite3': 'https://lycis.gitlab.io/nandus-archiv/data.sqlite3'
      console.log("Loading DB from: " + dbUrl)
      axios.get(dbUrl, {responseType: 'arraybuffer'})
                  .then((response) => {
                    console.log('begin http.get')
                    console.log(response.data)
                    console.log('end http.get')
                    let rawDataDBFile = new Uint8Array(response.data)

                    console.log('begin data')
                    console.log('data length ' + rawDataDBFile.length)
                    // console.log(mymdata)
                    console.log('end data')
                    let dataDBFile = []
                    for (let i = 0; i < rawDataDBFile.length; ++i) {
                      dataDBFile[i] = String.fromCharCode(rawDataDBFile[i])
                    }

                    commit('GET_DBFILE', dataDBFile.join(''))

                    self.DBFileIsLoaded()
                    console.log('end getDBFile in store')
                  })
                .catch((error) => {
                  console.log('begin error')
                  console.log(error)
                  console.log('end error')
                })
      console.log('end getDBFile')
    },
    nextChild ({ commit }, { idxChildNext }) {
      commit('NEXT_CHILD', idxChildNext)
    }
  }
})

app.provide("$regelWikiBaseUrl", "https://www.ulisses-regelwiki.de/")
app.provide("$store", store)
app.provide("test", "Hi!")
app.provide("queryDatabase", async function(sql) {
  let cobCon = await SQL({
    locateFile: file => `https://sql.js.org/dist/${file}`
  })

  console.log("sql query: " + sql)
  this.$store.state.db = new cobCon.Database(this.$store.state.dbFile)
  let rlt = this.$store.state.db.exec(sql)
  console.log(rlt)
  return rlt
})

app.use(createPinia())
app.use(router)

app.mount('#app')
