import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('../views/AboutView.vue')
    },
    {
      path: '/sf/profan-allgemein',
      name: 'Profan-Allgemein',
      component: () => import('../views/SFGeneric.vue')
    },
    {
      path: '/character/spezies',
      name: 'Spezies',
      component: () => import('../views/Spezies.vue')
    },
    {
      path: '/sf/kampf-allgemein',
      name: 'Profan-Kampf',
      component: () => import('../views/SFGeneric.vue')
    },
    {
      path: '/sf/alle',
      name: 'SFAlle',
      component: () => import('../views/SFAlle.vue')
    },
    {
      path: '/sf/kampf-alle',
      name: 'SFKampfAlle',
      component: () => import('../views/SFKampfAlle.vue')
    },
    {
      path: '/sf/kampf-stil',
      name: 'Profan-Kampfstil',
      component: () => import('../views/SFGeneric.vue')
    },
    {
      path: '/sf/kampf-eksf',
      name: 'Profan-Erweiterte Kampfsonderfertigkeit',
      component: () => import('../views/SFGeneric.vue')
    },
    {
      path: '/sf/kampf-pruegel',
      name: 'Profan-Prügel',
      component: () => import('../views/SFGeneric.vue')
    },
    {
      path: '/sf/profan-talentstil',
      name: 'Profan-Talentstil',
      component: () => import('../views/SFGeneric.vue')
    },
    {
      path: '/sf/profan-schicksal',
      name: 'Profan-Schicksalspunkte', 
      component: () => import('../views/SFGeneric.vue')
    },
    {
      path: '/sf/profan-talentsf',
      name: 'Profan-Erweiterte Talentsonderfertigkeit',
      component: () => import('../views/SFGeneric.vue')
    },
    {
      path: '/sf/magisch-allgemein',
      name: 'Magisch-Allgemein',
      component: () => import('../views/SFGeneric.vue')
    },
    {
      path: '/sf/magisch-traditionen',
      name: 'Magisch-Tradition',
      component: () => import('../views/SFGeneric.vue')
    },
    {
      path: '/impressum',
      name: 'Impressum',
      component: () => import('../views/Impressum.vue')
    },
    {
      path: '/sf/magisch-zauberstil',
      name: 'Magisch-Zauberstil',
      component: () => import('../views/SFGeneric.vue')
    },
    {
      path: '/sf/magisch-ezsf',
      name: 'Magisch-Erweiterte Zaubersonderfertigkeit',
      component: () => import('../views/SFGeneric.vue')
    },
    {
      path: '/sf/karmal-allgemein',
      name: 'Karmal-Allgemein',
      component: () => import('../views/SFGeneric.vue')
    },
    {
      path: '/sf/karmal-traditionen',
      name: 'Karmal-Tradition',
      component: () => import('../views/SFGeneric.vue')
    },
    {
      path: '/sf/karmal-liturgiestil',
      name: 'Karmal-Liturgiestil',
      component: () => import('../views/SFGeneric.vue')
    },
    {
      path: '/sf/karmal-eksf',
      name: 'Karmal-Erweiterte Liturgiesonderfertigkeit',
      component: () => import('../views/SFGeneric.vue')
    },
    {
      path: '/sf/karmal-predigt',
      name: 'Karmal-Predigt',
      component: () => import('../views/SFGeneric.vue')
    },
    {
      path: '/sf/karmal-vision',
      name: 'Karmal-Visions',
      component: () => import('../views/SFGeneric.vue')
    },
    {
      path: '/sf/magisch-lykantroph',
      name: 'Magisch-Lykanthropische Gabe',
      component: () => import('../views/SFGeneric.vue')
    },
    {
      path: '/sf/magisch-vampir',
      name: 'Magisch-Vampirische Gabe',
      component: () => import('../views/SFGeneric.vue')
    },
    {
      path: '/sf/magisch-sikaryan',
      name: 'Magisch-Sikaryan-Raub',
      component: () => import('../views/SFGeneric.vue')
    },
    {
      path: '/sf/pakt-daemon',
      name: 'Paktgeschenk-Dämonenpakt',
      component: () => import('../views/SFGeneric.vue')
    },
    {
      path: '/sf/pakt-fee',
      name: 'Paktgeschenk-Feenpakt',
      component: () => import('../views/SFGeneric.vue')
    },
    {
      path: '/sf/tiere-allgemein',
      name: 'Tiere-Allgemein',
      component: () => import('../views/SFGeneric.vue')
    },
    {
      path: '/sf/tiere-ausbildung',
      name: 'Tiere-Ausbildungsaufsatz',
      component: () => import('../views/SFGeneric.vue')
    },
    {
      path: '/sf/tiere-humunculus',
      name: 'Tiere-Humunculus',
      component: () => import('../views/SFGeneric.vue')
    },
    {
      path: '/sf/tiere-kampf',
      name: 'Tiere-Kampf',
      component: () => import('../views/SFGeneric.vue')
    },
    {
      path: '/sf/tiere-trick',
      name: 'Tiere-Trick',
      component: () => import('../views/SFGeneric.vue')
    },
    {
      path: '/sf/tiere-vertrautentrick',
      name: 'Tiere-Vertrautentrick',
      component: () => import('../views/SFGeneric.vue')
    },
    {
      path: '/sf/zz-allgemein',
      name: 'Zauberzeichen-Zauberzeichen',
      component: () => import('../views/SFGeneric.vue')
    },
    {
      path: '/sf/zz-ahnenzeichen',
      name: 'Zauberzeichen-Ahnenzeichen',
      component: () => import('../views/SFGeneric.vue')
    },
    {
      path: '/sf/zz-alhanisch',
      name: 'Zauberzeichen-Alhanishe Zauberzeichen',
      component: () => import('../views/SFGeneric.vue')
    },
    {
      path: '/sf/zz-kreise',
      name: 'Zauberzeichen-Bann- und Schutzkreise',
      component: () => import('../views/SFGeneric.vue')
    }, 
    {
      path:"/sf/traditionsartefakte",
      name: "Traditionsartefakte",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/traditionsartefakte-animisten",
      name: "Traditionsartefakte-Animistenwaffe",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/traditionsartefakte-druidendolch",
      name: "Traditionsartefakte-Druidendolch",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/traditionsartefakte-sichel",
      name: "Traditionsartefakte-Druidensichel",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/traditionsartefakte-bannschwert",
      name: "Traditionsartefakte-Bannschwert",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/traditionsartefakte-sichel",
      name: "Traditionsartefakte-Druidensichel",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/traditionsartefakte-gewand",
      name: "Traditionsartefakte-Gewandzauber",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/traditionsartefakte-sichel",
      name: "Traditionsartefakte-Druidensichel",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/traditionsartefakte-gmkristallkugel",
      name: "Traditionsartefakte-Gildenmagische Kugelzauber",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/traditionsartefakte-sichel",
      name: "Traditionsartefakte-Druidensichel",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/traditionsartefakte-instrument",
      name: "Traditionsartefakte-Instrumentzauber",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/traditionsartefakte-kappe",
      name: "Traditionsartefakte-Kappenzauber",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/traditionsartefakte-kessel",
      name: "Traditionsartefakte-Kesselzauber",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/traditionsartefakte-krallenkette",
      name: "Traditionsartefakte-Krallenkette",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/traditionsartefakte-lebensring",
      name: "Traditionsartefakte-Lebensring",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/traditionsartefakte-schale",
      name: "Traditionsartefakte-Schalenzauber",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/traditionsartefakte-srkristallkugel",
      name: "Traditionsartefakte-Scharlatanische Kugelzauber",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/traditionsartefakte-chronik",
      name: "Traditionsartefakte-Sippenchronik",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/traditionsartefakte-spielzeug",
      name: "Traditionsartefakte-Spielzeugzauber",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/traditionsartefakte-stab",
      name: "Traditionsartefakte-Stabzauber",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/traditionsartefakte-stecken",
      name: "Traditionsartefakte-Steckenzauber",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/traditionsartefakte-trinkhorn",
      name: "Traditionsartefakte-Trinkhornzauber",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst",
      name: "Zeremonialgegenstand",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst-achazkeule",
      name: "Zeremonialgegenstand-Achazkeule",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst-angroschanh",
      name: "Zeremonialgegenstand-Angroschanhänger",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst-avesstab",
      name: "Zeremonialgegenstand-Avesstab",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst-bds",
      name: "Zeremonialgegenstand-Buch der Schlange",
      component: () => import("../views/SFGeneric.vue")
    },
    {
      path:"/sf/zeremonialggst-efferd",
      name: "Zeremonialgegenstand-Efferdbart",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst-zssah",
      name: "Zeremonialgegenstand-Eidechsenkleidung",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst-nandus",
      name: "Zeremonialgegenstand-Einhornstirnband",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst-keule-ferkina",
      name: "Zeremonialgegenstand-Ferkinaknochenkeule",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst-firun",
      name: "Zeremonialgegenstand-Firunsmesser",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst-keule-fjarninger",
      name: "Zeremonialgegenstand-Fjarningerknochenkeule",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst-swafnir",
      name: "Zeremonialgegenstand-Flukenamulett",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst-travia",
      name: "Zeremonialgegenstand-Gänsebeutel",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst-fetisch",
      name: "Zeremonialgegenstand-Geisterfetisch",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst-keule-gjalsker",
      name: "Zeremonialgegenstand-Gjalskerknochenkeule",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst-peraine",
      name: "Zeremonialgegenstand-Grüne Handschuhe",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst-ifirn",
      name: "Zeremonialgegenstand-Ifirnsmantel",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst-ingerimm",
      name: "Zeremonialgegenstand-Ingerimmshammer",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst-kor",
      name: "Zeremonialgegenstand-Korspieß",
      component: () => import("../views/SFGeneric.vue")
    },
    {
      path:"/sf/zeremonialggst-marbo",
      name: "Zeremonialgegenstand-Marbodolch",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst-phex",
      name: "Zeremonialgegenstand-Mondamulett",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst-numinoru",
      name: "Zeremonialgegenstand-Muschelhorn",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst-keule-nivesen",
      name: "Zeremonialgegenstand-Nivesenknochenkeule",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst-namenlos",
      name: "Zeremonialgegenstand-Opferdolch des Namenlosen",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst-rondra",
      name: "Zeremonialgegenstand-Rondrakamm",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst-rahja",
      name: "Zeremonialgegenstand-Roter Schleier",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst-hszint",
      name: "Zeremonialgegenstand-Schlangenstab",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst-praios",
      name: "Zeremonialgegenstand-Sonnenzepter",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst-keule-tahaya",
      name: "Zeremonialgegenstand-Tahayaknochenkeule",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst-keule-tairach",
      name: "Zeremonialgegenstand-Tairachknochenkeule",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst-keule-trollz",
      name: "Zeremonialgegenstand-Trollzackerknochenkeule",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst-levthan",
      name: "Zeremonialgegenstand-Widderkeule",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst-tsa",
      name: "Zeremonialgegenstand-Prisma",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst-boron",
      name: "Zeremonialgegenstand-Rabenschnabel",
      component: () => import("../views/SFGeneric.vue")
    }, 
    {
      path:"/sf/zeremonialggst-swafnir2",
      name: "Zeremonialgegenstand-Walschild",
      component: () => import("../views/SFGeneric.vue")
    },
    {
      path: "/character/kulturen",
      name: "Kulturen",
      component: () => import("../views/Cultures.vue")
    },
    {
      path:"/sf/traditionsartefakte-schuppenbeutel",
      name: "Traditionsartefakte-Schuppenbeutel",
      component: () => import("../views/SFGeneric.vue")
    },
    {
      path:"/sf/traditionsartefakte-echsenhaube",
      name: "Traditionsartefakte-Echsenhaube",
      component: () => import("../views/SFGeneric.vue")
    },
    {
      path:"/sf/traditionsartefakte-fluggeraet",
      name: "Traditionsartefakte-Fluggerät",
      component: () => import("../views/SFGeneric.vue")
    },
    {
      path:"/sf/traditionsartefakte-krstkristallkugell",
      name: "Traditionsartefakte-Kristallomantische Kugel",
      component: () => import("../views/SFGeneric.vue")
    },
    {
      path:"/magie/zauber",
      name: "Zauber",
      component: () => import("../views/Zauber.vue")
    },
    {
      path:"/magie/rituale",
      name: "Rituale",
      component: () => import("../views/Rituale.vue")
    },
    {
      path:"/magie/erweiterungen",
      name: "Zaubererweiterungen",
      component: () => import("../views/Zaubererweiterungen.vue")
    }

  ]
})

export default router
